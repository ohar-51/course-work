# OHAR 2019 StudentPassing Software Architecture Memo

## Architectural styles
- BaseLayer: framework achitecture 
    -> kehys erikoistetaan toimivaksi tuotteeksi
    ->rajapintatoteutus, peritytyminen, olioiden/komponenttien luonti
    ->koottava kehys (?)

- pipes and filter architecture
    -koostuu filter-komponenteista jotka tuottavat ja kuluttavat tietoalkioita 
    sekä komponentilta toisille kulkevista väylistä (pipes)

configuratio muuntelumekanismi
nodet, input tiedostot
perintä
rajapintaluokat StudentLayer


## Important class roles
- processor node?


## Protocols
- upd

### StudentPassingProtocol
Defines how student data is passed from a node to another.

BaseLayer 
    ProcessorNode
        *ProcessorNode is a central class in the architecture. It acts as the Filter
        in the Pipes & Filters architectural style.
           // Forward declarations
                class NetworkReader;
                class NetworkWriter;
                class DataHandler;
                class DataItem;
                class NodeConfiguration;
    Package
        *Package encapsulates the data send between the ProcessorNode objects as well
        as passing the data around within a ProcessorNode and the Handlers and readers/writers
        in the node.
    
    Datahandler
        *DataHandler is an abstract class for handling data arriving to a ProcessorNode.
        Datahandler consumes data packages the ProcessorNode offers to it.
    
    NetworkReader
    
    NetworkWriter
    
    ProcessorNodeObserver
    
    DataItem
        * An abstract class, representing the data items the applications can use.
        Application developers should subclass the DataItem class and reimplement
        the abstract methods.
        
    CanfiguaretionDataItem 
        *This class defines the configuration data items a ProcessorNode uses to
        configure itself. Configuration data items have a name and a value defining
        the configuration.
        
    DataReadObserver
        *Class reads Node configuration data from a file. The file name is usually provided
	    as the startup parameters of the app which instantiates the ProcessorNode. App then
        instantiates the Node and calls configure() function.
        
Harjoitus 6
Missä loggaus aloitetaan? Ei arkkitehtuurisesti tärkeä asia... 
main.cpp alustetaan logworker DefaultLogger LOG(info) jotta kirjaston toiminnallisuutta voidaan käyttää hyväksi.
datagram network using boost asio
nlohmann::json fot parsing and creating JSON

1. mitä internetin protokollaa käyttäen tietoa siirretään?
    -udp 
    udp::socket

2. mitkä luokkamallin luokat vastaavat tiedon siirrosta verkosta/verkkoon? erittele missä luokat sijaitsevat (base vai student).
    -base : networker, networkwriter, networkreader
upd::socket, Buffer(JOKU), Networkreader sisääntulevan lukemiseen

3. mitä StudentPassing:n ulkopuolisia kirjastoja käytetään verkkotiedonsiirron toteutuksessa?
    -boost::asio::ip::upd::socket
    (async_reserve_to
    async_send_to)

4. mikä on tiedon siirron formaatti? 
    -json, tekstitietoa

5. minkälaisia viestejä komponentit lähettävät toisilleen verkon yli?
    -udp-viestejä? dataitemistä muodostettuja viestejä 
    package, id 
    type, viestin tyyppi komentoja tai dataa
    payload (opiskelijan tietoja json muodossa)

6. mitä StudentPassing:n ulkopuolista kirjastoa käytetään konversion toteutuksessa?
    -Dataitemista tehtaisiin package #include <boost/algorithm/string.hpp>
    -#include <boost/uuid/uuid_io.hpp>
    nlohmann::json for parsing and creating JSON

7. mitkä luokkamallin luokat (BaseLayer & StudentLayer) vastaavat verkossa tapahtuvan tiedon siirron formaatin käsittelystä 
    (sovelluksen sisäisestä formaatista ulkoiseen konvertointi ja takaisin)?
    -Ainakin package? Datahandler? datahandler ottaa ulkopuolisia paketteja vastaan?
    BaseLayer package ja type
    StudentLayer payloadin sisältö
    NetWorkReader. buffer, merkkijono, nlohmann json olioksi, toteutetaan packagelle konversiometodit (OHARBase::Package) package.cpp from_json metodit
    Takaisin studentInputHandler luo OHARstudent olion
    Vastaus: NetworkReader, studentInputhandler, package, studenthandler yhdessä json kirjaston kanssa

8. missä vaiheessa/paikassa käynnistyy konversio ulkoisesta esitysmuodosta sisäiseen esitysmuotoon?
    -datahandler koska se vastaanottaa paketteja
    StudentNetInputHandler, ProcessorNode, nhlohmann::json, package

9. missä vaiheessa/paikassa käynnistyy konversio sisäisestä esitysmuodosta ulkoiseen esitysmuotoon?
    -package, StudentDataItem, nlohmann::json, StudentNetOutputHAndler, ProcessorNode

Inputilla handler sekä Outputilla handler

TOINEN OSIO
1.  mitkä luokkamallin luokat vastaavat tiedon siirrosta tiedostoista sovellukseen ja takaisin tiedostoihin?
        erittele luokat BaseLayer:ssä ja toisaalta StudentLayer:ssä
    -BaseLayer :  DataFileReader DataReaderObserver, ConfigurationFileReader
    -StudentLayer : StudentFileReader, DataFileObserver, StudentFileWriter, StudentWriterHandler

2.  mikä on tiedon siirron formaatti?
        tähän lisätietoa InstalledFiles -hakemiston README.md:stä
    -TSV tap-separated values

3.  mitä tietoja tiedostoista luetaan/kirjoitetaan?
    -Mitä tulevassa tiedossa on (ehheh) dataitemin tietoja
    konfiguraatiotietoja, avainarvopareja  tekstitiedossa

4.  missä vaiheessa/paikassa käynnistyy konversio ulkoisesta esitysmuodosta sisäiseen esitysmuotoon?
    -datahandler koska se vastaanottaa paketteja
    DataFileReader hoitaa tiedoston avaamisen, virheen käsittelyn, sisällön tyypin, luetaan silmukassa kaikki luettava, parse-metodin kutsu 
    abstraktiluokka-> ConfigurationDataItem parsii SetItemName, SetItemData
    DataFileReader ei osaa erotella nimiä, joten luodaan aliluokka joka erottelee ne ja sitten ConfiguratioDataItem parsii sen
    StudentFileReader-> studentDataItem parsittavaksi
    ReadFile painamalla opiskelijan tietojen lukeminen, käyttöliittymän puolella kutsuu configuratiota ConfigurationFileReader, josta haetaan avainarvopareja

5.  missä vaiheessa/paikassa käynnistyy konversio sisäisestä esitysmuodosta ulkoiseen esitysmuotoon?
    -package
    HandleCommand säie hoitaa käsittelyn komentojakäsittelevä säie PlainStudentFIleHandler StudentFIleReader 
    Käyttäjä painaa read file -nappia

Dokumentoi StudentPassing:n virheiden käsittelyn sijoittamisen periaatteet arkkitehtuurimuistiinpanoihinne:

    ##missä virheet käsitellään ja miksi siellä?
    Elmain.cpp (main) Käynnistää ProcessorNode, jos käynnistyksessä tulee ongelma niin se ilmoittaa siitä
    ProcessorNode.cpp (suurinosa) 
        42: Jos sisäisten objektien poistossa tulee virhe niin se laittaa logiin tietoa siitä
        74: configure -metodi jos konfiguroinnissa tulee virhe niin se laittaa logiin ja laittaa virheilmoituksen käyttäjälle
        275: Start-metodi, jos starttaus ei onnistu niin pysäyttää toiminnon ja merkkaa logiin sekä antaa virheilmoituksen
        351: start-metodi commandHandlerThread tarkistaa komennot ja komennot eivät onnistu niin merkkaa logiin ja antaa virheilmotus
        576: passToHandler jos handler ei onnistu niin merkkaa logiin ja antaa virheilmoituksen
    NetWorkReader.cpp
        HandleReceive parsitaan tuleva json jos inputin parsimisessa tulee virhe niin ilmoittaa observerille että tuli virhe
    DDirWatcherHandler.cpp
        
    ##miten sovellus käyttäytyy jos verkosta tuleva data on virheellistä?
    HandleReceive parsitaan tuleva json jos inputin parsimisessa tulee virhe niin ilmoittaa observerille että tuli virhe
     
    ##miten sovellus käyttäytyy jos tiedostosta luettu data on virheellistä tai tiedostoa ei ole?
    Laittaa logiin varoituksen, poistaa itemin ja heittää errorin käyttäjälle, jos tiedostoa ei ole merkkaa logiin varoituksen ja palauttaa falsen 
    
    ##miten virheiden käsittelypaikat liittyvät edellisen tehtävän prosessointiarkkitehtuuriin eli ajonaikaiseen arkkitehtuuriin?
    Niissä metodeissa joissa otetaan inputtia vastaan on tärkeä tarkistaa että vastaanotettu data on validia, jotta virheet ei etene kovin pitkälle

## Design patterns used in StudentPassing

### Prototype
Used in BaseLayer/include/OHARBaseLayer/DataItem.h
To avoid the inherent cost of creating a new DataItem in the standard way (new keyword). Using method clone() (declared in StudentDataItem) to create new DataItem instantiations and to move DataItem objects to another places.

### Strategy
Used in grade calculations
To bring variations to grade calculations

### Message queue
Used in Networking (BaseLayer NetworkerReader and NetworkWriter)
To handle incoming and outgoing messages and avoid message overload
Quality requirement: software should function seamlessly even under heavy message load

### Factory method
Used in GradeFactory
To specify different grade calculators

### MVC
Used in user interface
To present different views of the program state and make UI modifiable
Project requirement: Scalable, movable
Quality requirement: 

### Template
Used in file readers
To read different types of data from a file
Project requirement: Re-use of software